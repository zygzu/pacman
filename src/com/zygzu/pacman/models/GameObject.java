package com.zygzu.pacman.models;

import java.awt.*;

public interface GameObject {

    void tick();

    void render(Graphics g);

}
