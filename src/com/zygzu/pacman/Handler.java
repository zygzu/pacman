package com.zygzu.pacman;

import com.zygzu.pacman.models.GameObject;

import java.awt.*;
import java.util.List;

public class Handler {

    private List<GameObject> objects;

    public void tick() {
        for(GameObject object : objects) {
            object.tick();
        }
    }

    public void render(Graphics g) {
        for(GameObject object : objects) {
            object.render(g);
        }
    }

}
