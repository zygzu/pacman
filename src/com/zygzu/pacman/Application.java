package com.zygzu.pacman;


import javax.swing.*;
import java.awt.*;

public class Application extends JFrame {

    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;
    private static final String TITLE = "Wielka kradzież samochodów";

    public static final double PROPORTIONS = 0.75;

    private Application() {
        init();
    }

    private void init() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setTitle(TITLE);

        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        add(new Screen());

        setVisible(true);
    }

    public static void main(String[] args) {
        new Application();
    }

}
