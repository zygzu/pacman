package com.zygzu.pacman;

import javax.swing.*;
import java.awt.*;

public class Screen extends JPanel implements Runnable {

    private static final int FPS = 60;

    private Thread gameThread;
    private boolean running;

    public Screen(){
        init();
    }

    private void init() {
        running = false;

        start();
    }

    private synchronized void start() {
        if(running) {
            return;
        }

        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    private synchronized void stop() {
        if(!running) {
            return;
        }

        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }

    @Override
    public void run() {
        long lastTime = System.nanoTime();
        double nsPerTick = 1000000000D / FPS;

        int ticks = 0;
        int frames = 0;

        long lastTimer = System.currentTimeMillis();
        double delta = 0;

        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerTick;
            lastTime = now;
            boolean shouldRender = false;

            while (delta >= 1) {
                ticks++;
                tick();
                delta -= 1;
                shouldRender = true;
            }

            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            if (shouldRender) {
                frames++;
                render();
            }

            if (System.currentTimeMillis() - lastTimer >= 1000) {
                lastTimer += 1000;
                System.out.println(ticks + " ticks, " + frames + " frames");
                frames = 0;
                ticks = 0;
            }
        }
    }

    private void tick() {

    }

    private void render() {
        repaint();
    }
}
